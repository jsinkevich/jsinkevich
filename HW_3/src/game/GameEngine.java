package game;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Random;

public class GameEngine {
    private boolean[][] patency;
    private char[][] graphicMap;
    private Person[] persons;

    public GameEngine() {
        patency = new boolean[10][10];
        graphicMap = new char[10][10];
        persons = new Person[3];
    }

    public void generateLevel() {
        for (int i = 0; i < 10; i++) {
            for (int j = 0; j < 10; j++) {
                patency[i][j] = true;
                graphicMap[i][j] = '.';
            }
        }
        Random rand = new Random();
        for(int i = 0; i < 30; i++) {
            int x = rand.nextInt(9);
            int y = rand.nextInt(9);
            patency[y][x] = false;
            graphicMap[y][x] = '#';
        }
        persons[0] = new Person(0, 9, '@', PersonType.Hero);
        persons[1] = new Person(9, 0, 'P', PersonType.Princess);
        persons[2] = new Person(rand.nextInt(9), rand.nextInt(9), 'M', PersonType.Monster);
        for (int i = 0; i < 3; i++) {
            patency[persons[i].getY()][persons[i].getX()] = true;
            graphicMap[persons[i].getY()][persons[i].getX()] = persons[i].getPortrait();
        }
    }

    public GameState turn() {
        int currentDirect = getDirection();
        if (currentDirect == 2) {
            return moveHero(persons[0].getX(), persons[0].getY() + 1);
        }
        if (currentDirect == 4) {
            return moveHero(persons[0].getX() - 1, persons[0].getY());
        }
        if (currentDirect == 6) {
            return moveHero(persons[0].getX() + 1, persons[0].getY());
        }
        if (currentDirect == 8) {
            return moveHero(persons[0].getX(), persons[0].getY() - 1);
        }
        return GameState.InProcess;
    }

    private GameState moveHero(int newX, int newY) {
        if ((newX >= 0) && (newX < 10) && (newY >= 0) && (newY < 10)) {
            if (this.patency[newY][newX]) {
                if ((newX == persons[2].getX()) && (newY == persons[2].getY())) {
                    return GameState.Defeat;
                }
                if ((newX == persons[1].getX()) && (newY == persons[1].getY())) {
                    return GameState.Victory;
                }
                persons[0].setX(newX);
                persons[0].setY(newY);
            }
        }
        draw();
        return GameState.InProcess;
    }

    private int getDirection() {
        int direction = 0;
        try {
            BufferedReader br = new BufferedReader(
            new InputStreamReader(System.in));
            direction = Integer.parseInt(br.readLine());
        } catch (Exception ex) {
            System.out.println("Ошибка ввода!!!");
        }
        return direction;
    }

    public void draw() {
        graphicMap[persons[0].getY()][persons[0].getX()] = persons[0].getPortrait();
        for (int i = 0; i < 20; i++) {
            System.out.println();
        }
        for (int i = 0; i < 10; i++) {
            for(int j = 0; j < 10; j++) {
                System.out.print(" "  + this.graphicMap[i][j]);
            }
            System.out.println();
        }
        graphicMap[persons[0].getY()][persons[0].getX()] = '.';
    }
}
