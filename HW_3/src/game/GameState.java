package game;

public enum GameState {
    InProcess,
    Defeat,
    Victory
}
