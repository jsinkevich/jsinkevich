package com.jsinkevich;

import fileSystem.FileEngine;

public class Main {

    public static void main(String[] args) {
        FileEngine fe = new FileEngine();
        fe.start();
    }
}
