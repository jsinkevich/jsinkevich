package fileSystem;

public class File extends FileNode {
    private String extension;

    public File(String name, String extension) {
        super(name);
        this.extension = extension;
    }

    @Override
    public String toString() {
        return this.name + "." + this.extension;
    }
}
