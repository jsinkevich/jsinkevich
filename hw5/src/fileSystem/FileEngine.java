package fileSystem;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class FileEngine {
    private Folder root;

    public FileEngine() {
        root = new Folder("root");
    }

    public void start() {
        String command = "";
        while (!command.equals("exit")) {
            System.out.print("$>");
            BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
            try {
                command = br.readLine();
            } catch (IOException e) {
                e.printStackTrace();
            }
            this.run(command);
        }
    }

    private String[] slashSplit(String str) {
        ArrayList<String> words = new ArrayList<String>();
        int lastSlashIndex = 0;
        for (int i = 0; i < str.length(); i++) {
            if (str.charAt(i) == '/') {
                words.add(str.substring(lastSlashIndex, i));
                lastSlashIndex = i + 1;
            }
        }
        words.add(str.substring(lastSlashIndex));
        String[] result = new String[words.size()];
        for (int i = 0; i < words.size(); i++) {
            result[i] = words.get(i);
        }
        return result;
    }

    public void run(String command) {
        if (command == "exit"){
            return;
        }

        if (command.equals("print")) {
            System.out.println(root.filePathPart(0));
            return;
        }

        String[] names = slashSplit(command);
        Folder targetFile = this.root;
        for (int i = 1; i < names.length - 1; i++) {
            targetFile = (Folder)targetFile.add(names[i]);
        }
        targetFile.add(names[names.length - 1]);
    }
}
