package fileSystem;

public class FileNode {
    protected String name;

    public FileNode(String name) {
        this.name = name;
    }

    public String getName() {
        return this.name;
    }

    public String toString() {
        return this.name;
    }

    public String filePathPart(int spacing) {
        String spaces = "";
        for (int i = 0; i < spacing; i++) {
            spaces += ' ';
        }
        return spaces + this.toString();
    }
}
