package fileSystem;

import java.util.ArrayList;

public class Folder extends FileNode {
    private ArrayList<FileNode> childs;

    public Folder(String name) {
        super(name);
        childs = new ArrayList<FileNode>();
    }

    public FileNode add(String fileName) {
        FileNode target = null;
        for (FileNode item : childs) {
            if (item.name.equals(fileName)) {
                target = item;
                break;
            }
        }
        if (target != null) {
            return target;
        }
        int dotIndex = -1;
        for (int i = 0; i < fileName.length(); i++) {
            if (fileName.charAt(i) == '.') {
                dotIndex = i;
            }
        }
        if (dotIndex == -1) {
            Folder folder = new Folder(fileName);
            this.childs.add(folder);
            return folder;
        } else {
            String name = fileName.substring(0, dotIndex);
            String extension = fileName.substring(dotIndex + 1);
            File file = new File(name, extension);
            this.childs.add(file);
            return file;
        }
    }

    @Override
    public String toString() {
        return this.name + "/";
    }

    @Override
    public String filePathPart(int spacing) {
        String result = super.filePathPart(spacing);
        for (FileNode item : this.childs) {
            result += "\n" + item.filePathPart(spacing + 2);
        }
        return result;
    }

    public ArrayList<FileNode> getChilds() {
        return this.childs;
    }
}
