package economy;

import com.sun.istack.internal.NotNull;

public class ATM {

    private Card card;
    private String name;

    public ATM(String name) {
        this.name = name;
    }

    public void insertCard(@NotNull Card card) {
        this.card = card;
    }

    public void putCash(double value) throws CardNotInsertedException {
        if (card == null) {
            throw (new CardNotInsertedException());
        } else {
            card.setAccount(card.getAccount() + value);
        }
    }

    public void withdrawCash(double value) throws CardNotInsertedException {
        if (card == null) {
            throw (new CardNotInsertedException());
        } else {
            card.setAccount(card.getAccount() - value);
        }
    }

    public String getName() {
        return name;
    }

    public Card getCard() {
        return card;
    }
}
