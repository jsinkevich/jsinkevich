package economy;

public class ATMRuler implements Runnable {

    private ATM atm;
    private boolean isProducer;

    public ATMRuler(ATM atm, boolean isProducer) {
        super();
        this.atm = atm;
        this.isProducer = isProducer;
    }

    @Override
    public void run() {
        Thread thread = Thread.currentThread();
        while (true) {
            long waitTime = 1000 + (long) (Math.random() * 1000);//1 - 2 секунды
            try {
                thread.sleep(waitTime);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            if (atm.getCard().getAccount() < 0) {
                break;
            }
            if (atm.getCard().getAccount() > 1000) {
                break;
            }
            double cashValue = 5 + Math.random() * 10;//5 - 15 долларов
            if (isProducer) {
                try {
                    atm.putCash(cashValue);
                    System.out.println("Банкомат " + atm.getName() + " пополнил счет карты на " + (Math.round(cashValue * 100) / 100) + "$");
                    System.out.println("Текущий счет карты " + atm.getCard().getName() + " " + (Math.round(atm.getCard().getAccount() * 100) / 100) + "$");
                } catch (CardNotInsertedException e) {
                    e.printStackTrace();
                }
            } else {
                try {
                    atm.withdrawCash(cashValue);
                    System.out.println("Банкомат " + atm.getName() + " снял со счета карты " + (Math.round(cashValue * 100) / 100) + "$");
                    System.out.println("Текущий счет карты " + atm.getCard().getName() + " " + (Math.round(atm.getCard().getAccount() * 100) / 100) + "$");
                } catch (CardNotInsertedException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
