package economy;

public class Card {

    private double account;
    private String name;

    public Card(String name, double cash) {
        this.name = name;
        this.account = cash;
    }

    public String getName() {
        return name;
    }

    public double getAccount() {
        return account;
    }

    public synchronized void setAccount(double value) {
        account = value;
    }
}
