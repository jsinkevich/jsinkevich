package economy;

import org.junit.Assert;
import org.junit.Test;

public class ATMTest {

    @Test
    public void putZeroCashTest() throws CardNotInsertedException {
        Card card = new Card("1", 0);
        ATM atm = new ATM("a");
        atm.insertCard(card);
        atm.putCash(0);
        Assert.assertEquals(0, atm.getCard().getAccount(), 0.000001);
    }

    @Test
    public void putTenCashTest() throws CardNotInsertedException {
        Card card = new Card("1", 1);
        ATM atm = new ATM("a");
        atm.insertCard(card);
        atm.putCash(10);
        Assert.assertEquals(11, atm.getCard().getAccount(), 0.000001);
    }

    @Test
    public void withdrawZeroCashTest() throws CardNotInsertedException {
        Card card = new Card("1", 0);
        ATM atm = new ATM("a");
        atm.insertCard(card);
        atm.withdrawCash(0);
        Assert.assertEquals(0, atm.getCard().getAccount(), 0.000001);
    }

    @Test
    public void withdrawTenCashTest() throws CardNotInsertedException {
        Card card = new Card("1", 11);
        ATM atm = new ATM("a");
        atm.insertCard(card);
        atm.withdrawCash(10);
        Assert.assertEquals(1, atm.getCard().getAccount(), 0.000001);
    }
}